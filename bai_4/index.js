function submit() {
  function createDiv() {
    let ketQua = "";
    for (let i = 1; i <= 10; i++) {
      if (i % 2 === 0) {
        //chẵn
        ketQua += `<div class="alert alert-primary">Div chẵn:${i}</div>`;
      } else {
        //lẻ
        ketQua += `<div class="alert alert-danger">Div lẻ: ${i}</div>`;
      }
    }
    return ketQua;
  }

  const ketQua = createDiv();
  document.getElementById("result").innerHTML = ketQua;
  //   createDivResultEL.innerHTML = `<p class='mb-3'>Kết quả: </p>${result}`;
}
